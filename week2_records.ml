(* Records *)

type point  = { x : float; y : float; z : float }
type dpoint = { dx : float; dy : float; dz : float }
type physical_object = { position : point; velocity : dpoint }

let move p dp =
  {x = (p.x +. dp.dx);
   y = (p.y +. dp.dy);
   z = (p.z +. dp.dz)} ;;

let next obj =
  {position = (move obj.position obj.velocity);
   velocity = obj.velocity} ;;

let distance_between_points p1 p2 =
  let dx = (p1.x -. p2.x) in
  let dy = (p1.y -. p2.y) in
  let dz = (p1.z -. p2.z) in
  sqrt ((dx *. dx) +. (dy *. dy) +. (dz *. dz))

let spheres_collide p1 p2 radius =
  let distance = distance_between_points p1 p2 in
  distance < radius +. radius

let will_collide_soon p1 p2 =
  let np1 = (next p1).position in
  let np2 = (next p2).position in
  spheres_collide np1 np2 1.0;;
