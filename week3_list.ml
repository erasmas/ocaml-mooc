type 'a clist =
  | CSingle of 'a
  | CApp of 'a clist * 'a clist
  | CEmpty

let example =
  CApp (CApp (CSingle 1, CSingle 2),
        CApp (CSingle 3,
              CApp (CSingle 4, CEmpty)))

let to_list l =
  let rec to_list' acc = function
    | CEmpty -> acc
    | CSingle a -> acc @ [a]
    | CApp (l, r) -> acc @ (to_list' [] l) @ (to_list' [] r) in
  to_list' [] l

let rec of_list = function
  | [] -> CEmpty
  | [a] -> CSingle a
  | a :: b :: tl -> CApp (CApp (CSingle a, CSingle b), of_list tl)

let append l1 l2 =
  match (l1, l2) with
  | CEmpty, l -> l
  | l, CEmpty -> l
  | l1, l2 -> CApp (l1, l2)

let rec hd = function
  | CEmpty -> None
  | CSingle x -> Some x
  | CApp (CEmpty, r) -> hd r
  | CApp (l, CEmpty) -> hd l
  | CApp (l, r) ->
    let hd_l = hd l in
    if hd_l <> None then hd_l
    else hd r

let rec is_empty = function
  | CEmpty -> true
  | CSingle _ -> false
  | CApp (CEmpty, CEmpty) -> true
  | CApp (CEmpty, r) -> is_empty r
  | CApp (l, CEmpty) -> is_empty l
  | _ -> false
;;

let rec tl = function
  | CEmpty -> None
  | CSingle _ -> Some CEmpty
  | CApp (CEmpty, x) -> tl x
  | CApp (l, r) ->
    match (tl l) with
    | Some l' -> Some (append l' r)
    | None -> tl r
;;
