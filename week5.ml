(* ... *)

type report = message list
and status = Successful | Failed
and message = string * status

let exec f x =
  try Ok (f x)
  with exn -> Error exn
;;

let compare user reference to_string : message =
  match user, reference with
  | Ok u, Ok r when u=r -> "got correct value " ^ to_string u, Successful
  | Ok u, _ -> "got unexpected value " ^ to_string u, Failed
  | Error u, Error r when u=r -> "got correct exception " ^ (to_string u), Successful
  | Error u, _ -> "got unexpected exception " ^ (to_string u), Failed
;;

let rec range ?(start=0) len =
    if start >= len
    then []
    else start :: (range len ~start:(start+1))
;;

let test user reference sample to_string =
  List.map (fun _ ->
      let test_data = sample () in
      let actual = exec user test_data
      and expected = exec reference test_data
      in compare actual expected to_string) (range 10)
;;

(* ... *)

let int_print i =
  print_string ((string_of_int i) ^ "\n")
;;

let print_int_list l =
  List.iter int_print l
;;

let print_every_other k l =
  List.iteri (fun i e -> if ((i mod k) = 0) then (int_print e)) l
;;

let print_list print l =
  List.iter (fun i -> print i ; print_string "\n") l
;;

(* PRINTING WITH LOOPS *)

let output_multiples x n m =
  for i = n to m do
    if (i mod x) = 0 then Printf.printf "%d," i
  done
;;

exception Zero_value

let display_sign_until_zero f m =
  try
    for i = 0 to m do
      let x = f i in
      if x < 0 then
        print_endline "negative"
      else if x > 0 then
        print_endline "positive"
      else
        raise (Zero_value)
    done
  with
  | Zero_value -> print_endline "zero"
;;

(* PRODUCING FINE ASCII ART *)

type image = int -> int -> bool ;;

let all_white = fun x y -> false ;;

let all_black = fun x y -> true ;;

let checkers = fun x y -> y/2 mod 2 = x/2 mod 2 ;;

let square cx cy s = fun x y ->
  let minx = cx - s / 2 in
  let maxx = cx + s / 2 in
  let miny = cy - s / 2 in
  let maxy = cy + s / 2 in
  x >= minx && x <= maxx && y >= miny && y <= maxy
;;

let disk cx cy r = fun x y ->
  let x' = x - cx in
  let y' = y - cy in
  (x' * x' + y' * y') <= r * r
;;

type blend =
  | Image of image
  | And of blend * blend
  | Or of blend * blend
  | Rem of blend * blend
;;

let black = "#"
let white = " "

let display_image width height f_image =
  for y = 0 to width do
    for x = 0 to height do
      let is_black = f_image x y in
      if is_black then
        print_string black
      else
        print_string white
    done;
    print_endline ""
  done
;;

let rec render blend x y =
  match blend with
  | Image f -> f x y
  | And (f, g) -> render f x y && render g x y
  | Or (f, g) -> render f x y || render g x y
  | Rem (f, g)  -> (render f x y) && not (render g x y)
;;

let display_blend width height blend =
  display_image width height (render blend)
;;

(* Mutable Arrays *)

let rotate a =
  let n = Array.length a in
  if n > 1 then
    let v = a.(0) in
    for i = 0 to n-2 do
      a.(i) <- a.(i+1)
    done;
    a.(n-1)<-v ;;

let rotate_by a n =
  let asize = Array.length a in
  if asize > 1 then
    let abs_n = abs n in
    let m     = if abs_n > asize then abs_n mod asize else abs_n in
    let moves = if (n < 0) then asize - m else m in
    for i = 1 to moves do
      rotate a
    done ;;

(* Implementing a stack with an array *)

type stack = int array
exception Full
exception Empty

let create size =
  Array.make (size + 1) 0 ;;

let push buf elt =
  let capacity = (Array.length buf) - 1 in
  let size = Array.get buf 0 in
  if size < capacity then
    let _ = buf.(size + 1) <- elt in
    buf.(0) <- (size + 1)
  else
    raise Full
;;

let append buf arr =
  let i = ref ((Array.length arr) - 1) in
  while !i >= 0 do
    push buf arr.(!i);
    decr i
  done
;;

let pop buf =
  let size = Array.get buf 0 in
  if (size = 0) then
    raise Empty
  else
    buf.(0) <- (size - 1);
    buf.(size)
;;

(* Implementing mutable lists *)

type 'a xlist =
  { mutable pointer : 'a cell }
and 'a cell =
  | Nil
  | List of 'a * 'a xlist ;;

let nil () =
  { pointer = Nil } ;;

let cons elt rest =
  { pointer = List (elt, rest) } ;;

exception Empty_xlist ;;

let head (l : 'a xlist) : 'a =
  match l with
  | { pointer = Nil } -> raise Empty_xlist
  | { pointer = List (elt, _) } -> elt ;;

let tail (l : 'a xlist) : 'a xlist =
  match l with
  | { pointer = Nil } -> raise Empty_xlist
  | { pointer = List (_, rest) } -> rest ;;

let add a l =
  let rec aux a l =
    match l.pointer with
    | Nil -> { pointer = List (a, { pointer=Nil }) }
    | List (e, tail) ->
        { pointer = List (a, aux e tail) } in
  l.pointer <- (aux a l).pointer

let chop l =
  let rec aux l =
    match l.pointer with
    | Nil -> raise Empty_xlist
    | List (e, tail) -> tail in
  l.pointer <- (aux l).pointer ;;

let append l l' =
  let rec last l =
  match l.pointer with
  | Nil -> l
  | List (e, tail) -> last tail in
  let p = last l in
  p.pointer <- l'.pointer
;;

let rec filter p l =
  match l.pointer with
  | Nil -> ();
  | List (elt, rst) -> if p elt then filter p rst
    else let _ = chop l in filter p l
;;

(* Simple uses of references *)

exception Empty ;;

let swap ra rb =
  let tmp = !ra in
  ra := !rb;
  rb := tmp
;;

let update r f =
  let v  = !r in
  let v' = f v in
  r := v';
  v
;;

let move l1 l2 =
  match !l1 with
  | [] -> raise Empty
  | l :: ls ->
    l1 := ls;
    l2 := l :: !l2
;;

let reverse l =
  let l'  = ref l
  and rl = ref [] in
  try
    while true do
      move l' rl
    done;
    !rl
  with
  | Empty -> !rl;
;;

(* Reading lines from the standard input  *)

let read_lines =
  let rec aux lines () =
    try
      aux (read_line () :: lines) ()
    with
      End_of_file -> List.rev lines in
  aux []
;;
