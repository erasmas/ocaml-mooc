type index = Index of int

let read a index =
  match index with
  | Index i -> a.(i) ;;

let inside a index =
  match index with
  | Index i -> if (i >= 0 && i < Array.length a) then true else false;;

let next index =
  match index with
  | Index i -> Index (i + 1);;

let min_index a =
  let rec loop min_index index =
    match index with
    | Index i ->
      if inside a index then
        if read a index < read a min_index
        then loop index (next index)
        else loop min_index (next index)
      else min_index in
  loop (Index 0) (Index 0)


(* Option type  *)

let find a w =
  let rec loop i =
    if i >= Array.length a then
      None
    else if a.(i) = w then
      Some i
    else loop (i + 1) in
  loop 0

let default_int = function
  | Some x -> x
  | None -> 0;;

let merge a b =
  match a, b with
  | None, None -> None
  | (Some x), None -> Some x
  | None, (Some x) -> Some x
  | (Some x), (Some y) -> Some (x + y)
