type 'a bt =
  | Empty
  | Node of 'a bt * 'a * 'a bt ;;

let height t =
  let rec height' t h =
    match t with
    | Empty -> h
    | Node (l, _, r) ->
      max (height' l (h + 1)) (height' r (h + 1)) in
  height' t 0

let rec balanced = function
  | Empty -> true
  | Node (Empty, _, Empty) -> true
  | Node (Empty, _, r) -> false
  | Node (l, _, Empty) -> false
  | Node (l, _, r) -> balanced l && balanced r ;;
