type queue = int list * int list

let is_empty = function
  | ([], []) -> true
  | _ -> false;;

let enqueue x (front, back) =
  (front, x :: back);;

let split = function
  | [] -> ([], [])
  | x :: [] -> ([x],[])
  | (x :: xs) as l ->
    let n = (List.length xs) / 2 + 1 in
    let rec aux i acc = function
      | [] -> List.rev acc, []
      | h :: t as l -> if i = 0 then (List.rev l, List.rev acc)
        else aux (i-1) (h :: acc) t in
    aux n [] l;;

let rec dequeue = function
  | (f :: fs, back) -> (f, (fs, back))
  | ([], back) ->
    match split (List.rev back) with
    | (f::[], back) -> (f, ((List.rev back), []))
    | (front, back) as q' -> dequeue q'

let rec mem (n : int) (xs : int list) : bool =
  match xs with
  | [] -> false
  | x :: xs -> if x = n then true else mem n xs

let rec append l1 l2 =
  match l1 with
  | [] -> l2
  | hd :: tl -> hd :: (append tl l2)

let combine l1 l2 : (int * int) list =
  let rec aux acc l1 l2 =
    match (l1, l2) with
    | ([], _)
    | (_, []) -> acc
    | (x :: xs, y :: ys) -> aux (acc @ [x, y]) xs ys in
  aux [] l1 l2

let rec assoc l key =
  match l with
  | [] -> None
  | (k, v) :: xs -> if k = key then Some v else assoc xs key


(* Symbolic manipulation of arithmetic expressions *)

type exp =
  | EInt of int
  | EAdd of exp * exp
  | EMul of exp * exp

let example =
  EAdd (EInt 1, EMul (EInt 2, EInt 3))

let my_example =
  EAdd
    (EMul (EInt 2, EInt 2),
     EMul (EInt 3, EInt 3))

let rec eval = function
  | EInt x -> x
  | EAdd (x, y) -> eval x + eval y
  | EMul (x, y) -> eval x * eval y

let factorize = function
  | EAdd (EMul (e1, e2), (EMul (e1', e2'))) as exp ->
    if e1 = e1' then EMul (e1, EAdd (e2, e2'))
    else if e1 = e2' then EMul (e1, EAdd (e1', e2))
    else
      exp
  | exp -> exp

let expand = function
  | EMul (a, EAdd (b, c)) ->
    EAdd (EMul (a, b), EMul (a, c))
  | exp -> exp

let simplify = function
  | EMul (a, EInt 0) -> EInt 0
  | EMul (a, EInt 1) -> a
  | EAdd (a, EInt 0) -> a
  | EMul (EInt 0, a) -> EInt 0
  | EMul (EInt 1, a) -> a
  | EAdd (EInt 0, a) -> a
  | exp -> exp
