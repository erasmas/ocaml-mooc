type filesystem =
  (string * node) list
and node =
  | File
  | Dir of filesystem
  | Symlink of string list

let file_prefix = "| "
and symlink_prefix = " -> "
and dir_prefix = "/"

let rec applyn n f v =
  if n = 0 then v else applyn (n - 1) f (f v)

let repeat_string n s =
  applyn n (fun sb -> sb ^ s) "";;

let print_path path =
  print_string (String.concat "/" path)
;;

let prefix_string lvl prefix s =
  (repeat_string lvl prefix) ^ s ;;

let print_file lvl name =
  print_endline (prefix_string lvl file_prefix name) ;;

let symlink_path paths =
  String.concat dir_prefix paths

let print_symlink lvl name paths =
  let level = prefix_string lvl file_prefix name in
  let path  = symlink_path paths in
  Printf.printf("%s -> %s\n") level path ;;

let rec print_dir lvl name =
  let level = prefix_string lvl file_prefix dir_prefix in
  print_endline (level ^ name) ;;

let resolve sym path =
  let rec resolve acc path =
    match (path, acc) with
    | ([], _) -> List.rev acc
    | (p :: tl, []) when p = ".." -> resolve [] tl
    | (p :: tl, _ :: xs) when p = ".." -> resolve xs tl
    | (p :: tl, acc) -> resolve (p :: acc) tl in
  resolve (List.tl (List.rev sym)) path ;;

let rec file_exists fs path =
  match (fs, path) with
  | ([], []) -> true
  | ((f, File) :: fx, p :: ps) when f = p -> file_exists fx ps
  | ((f, Dir d) :: fx, p :: ps) when f = p -> file_exists d ps
  | ((f, _) :: fx, path) -> file_exists fx path
  | (_, _) -> false
;;

let print_filesystem root =
  let rec print_filesystem lvl items cur_path =
    match items with
      [] -> ()
    | (name, File) :: tl ->
      print_file lvl name;
      print_filesystem lvl tl cur_path;

    | (name, Dir fs) :: tl ->
      print_dir lvl name;
      print_filesystem (lvl + 1) fs (name :: cur_path);
      print_filesystem lvl tl cur_path

    | (name, Symlink fs) :: tl ->
      let link_path = (List.rev (name :: cur_path)) in
      let resolved_link = resolve link_path fs in
      match file_exists root resolved_link with
      | true ->
        print_symlink lvl name fs;
        print_filesystem lvl tl cur_path
      | _    ->
        print_symlink lvl name ["INVALID"];
        print_filesystem lvl tl cur_path
  in
  print_filesystem 0 root [];;
