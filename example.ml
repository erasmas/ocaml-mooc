(* Binary tree with leaves car­rying an integer. *)
type tree = Leaf of int | Node of tree * tree

let rec exists_leaf test tree =
  match tree with
  | Leaf v -> test v
  | Node (left, right) ->
      exists_leaf test left
      || exists_leaf test right

let has_even_leaf tree =
  exists_leaf (fun n -> n mod 2 = 0) tree

let multiple_of n d =
  (n mod d) = 0

let integer_square_root n =
  int_of_float(sqrt(float_of_int(n)))

let last_character str =
  let index = (String.length str) - 1 in
  String.get str index

let string_of_bool truth =
  match truth with
    true -> "true"
  | false -> "false"

let rec gcd a b =
  match (a mod b) with
    0 -> b
  | r -> gcd b r

let rec multiple_upto n r =
  match r with
    1 -> false
  | d -> if (n mod d) = 0
    then true
    else multiple_upto n (d - 1);;

let is_prime n =
  let n = abs n in
  let rec is_not_divisor k =
    k * k > n || (n mod k <> 0 && is_not_divisor (k + 1) ) in
  n <> 1 && is_not_divisor 2;;
