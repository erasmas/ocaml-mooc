(* A phone number is a sequence of four integers. *)
type phone_number = int * int * int * int;;

(* A contact has a name and a phone number. *)
type contact = {
  name         : string;
  phone_number : phone_number
};;

(* Here is a dumb contact. *)
let nobody = { name = ""; phone_number = (0, 0, 0, 0) };;

(* A database is a collection of contacts. *)
type database = {
  number_of_contacts : int;
  contacts : contact array;
};;

(* [make n] is the database with no contact and at most [n] contacts
    stored inside. *)
let make max_number_of_contacts =
  {
    number_of_contacts = 0;
    contacts = Array.make max_number_of_contacts nobody
  };;

(* Queries are represented by a code and a contact.
   - If the code is 0 then the contact must be inserted.
   - If the code is 1 then the contact must be deleted.
   - If the code is 2 then we are looking for a contact
     with the same name in the database. *)
type query = {
  code    : int;
  contact : contact;
}

let search db contact =
  let rec aux idx =
    if idx >= db.number_of_contacts then
      (false, db, nobody)
    else if db.contacts.(idx).name = contact.name then
      (true, db, db.contacts.(idx))
    else
      aux (idx + 1)
  in
  aux 0;;

let insert db contact =
  if db.number_of_contacts >= Array.length db.contacts then
    (false, db, nobody)
  else
    let (status, db, _) = search db contact in
    if status then (false, db, contact) else
      let cells i =
	if i = db.number_of_contacts then contact else db.contacts.(i)
      in
      let db' = {
        number_of_contacts = db.number_of_contacts + 1;
          contacts = Array.init (Array.length db.contacts) cells
        }
      in
      (true, db', contact);;

let print_contact {name=name; phone_number=(a,b,c,d)} =
  Printf.printf "{ name='%s'; phone_number='(%d,%d,%d,%d)'}\n" name a b c d

let sort_contacts a : contact array =
  ((Array.sort (fun a b -> if a = nobody then 1 else -1) a); a);;

let delete db contact =
  let (status, db, contact) = search db contact in
  if not status then (false, db, contact)
  else
    let cells i =
      if db.contacts.(i).name = contact.name then
        nobody
      else
        db.contacts.(i) in
    let contacts = Array.init (Array.length db.contacts) cells in
    let db' = {
        number_of_contacts = db.number_of_contacts - 1;
        contacts = sort_contacts contacts
      }
    in
    (true, db', contact);;

(* let contact_exists contact a = *)
(*   Array.exists (fun c -> c = contact) a *)

let contact_exists contact a =
  let rec loop i =
    if i >= Array.length a then
      false
    else if a.(i) = contact then
      true
    else loop (i + 1) in
  loop 0

let update db contact =
  if contact_exists contact db.contacts then
    (false, db, contact)
  else
    let (_, db', _) = delete db contact in
    (insert db' contact)

(* Engine parses and interprets the query. *)
let engine db query = function
  | Delete -> delete db contact
  | Insert -> insert db contact
  | Search -> search db contact
  | Update -> update db contact ;;

let execute (db : database) (queries : query array) =
  let rec loop result qs =
    match qs with
    | [] -> result
    | q :: tl ->
      let (_, db, _) = result in
      loop (engine db q) tl in
  loop (false, db, nobody) (Array.to_list queries)

let proof_of_bug : (query array) =
  [| {code = 0; contact = {name="Bob"; phone_number=(1,1,1,1)} };
     {code = 0; contact = {name="Alice"; phone_number=(2,2,2,2)} };
     {code = 1; contact = {name="Bob"; phone_number=(1,1,1,1)} };
     {code = 0; contact = {name="Alice"; phone_number=(3,3,3,3)} }
  |]
