let min (a : int array) : int =
  Array.fold_left (fun a b -> if a < b then a else b) (Array.get a 0) a

let is_sorted (a : string array ) : bool =
  let array_size = Array.length a in
  let rec loop i : bool =
    if i = array_size - 1
    then true
    else
      let s1, s2 = (Array.get a i), Array.get a (i + 1) in
      if ((String.compare s1 s2) = (-1))
      then loop (i + 1)
      else false in
  if array_size = 0 then true else loop 0

let find (dict: string array) (word : string) : int =
  let rec bs min max =
    if min >= max then
      -1
    else
      (* mid is always strictly less than max because min < max. *)
      let mid = (min + max) / 2 in
      let s = (Array.unsafe_get dict mid) in
      let diff = String.compare word s in
      if diff = 0 then mid else
      if diff < 0
      then bs min mid
      else bs (mid + 1) max
  in bs 0 (Array.length dict);;
