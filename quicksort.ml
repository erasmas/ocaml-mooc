type 'a cont =
  | Left of 'a list * 'a * 'a cont
  | Return

let rec quicksort list accu cont =
  match list with
  | [] -> eval_cont cont accu
  | element::[] -> eval_cont cont (element::accu)
  | pivot::rest ->
    let left, right = List.partition (fun element -> element < pivot) rest in
    quicksort right accu (Left (left, pivot, cont))

and eval_cont = function
  | Left (left, pivot, cont) ->
    (fun sorted_right -> quicksort left (pivot :: sorted_right) cont)
  | Return -> (fun x -> x)

let qsort li = quicksort li [] Return

(* time qsort (generate_list 1000000 100);; *)
(* execution elapsed time: 367.478062 sec *)
