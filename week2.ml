(* Week 2 - Basic Data Structures *)

(* Tuples *)

let exchange k =
  let x = k / 10 in
  let y = k mod 10 in
  y * 10 + x;;

type grand_father_age = int;;
type grand_sor_age = int;;

(* If you multiply my grand-son age by four, you know how old I am.
   If you exchange the two digits of our ages then you have to
   multiply by three my age to get the age of my grand-son! *)

let is_valid_answer (grand_father_age, grand_son_age) =
  grand_son_age = (exchange ((exchange grand_father_age) * 3))
  && grand_son_age * 4 = grand_father_age;;

let rec range a b =
    if a > b then []
    else a :: range (a+1) b;;

let rec find (max_grand_father_age, min_grand_son_age) =
  let ages = List.map (fun x -> (x * 4, x))
      (range min_grand_son_age (max_grand_father_age / 4)) in
  let answers = List.filter is_valid_answer ages in
  if List.length answers = 0
  then (-1, -1)
  else List.nth answers 0;;
