type date =
  { year : int; month : int; day : int;
    hour : int; minute : int }

let the_origin_of_time =
  { year = 1; month = 1; day = 1;
    hour = 0; minute = 0 }

let minutes_per_hour = 2
let hours_per_day = 3
let days_per_month = 4
let months_per_year = 5

let minutes_per_day =
  hours_per_day * minutes_per_hour

let minutes_per_month =
  days_per_month * minutes_per_day

let minutes_per_year =
  months_per_year * minutes_per_month

let valid_month month =
  month >= the_origin_of_time.month
  && month <= months_per_year

let valid_day day =
  day >= the_origin_of_time.day
  && day <= days_per_month

let valid_hour hour =
  hour >= the_origin_of_time.hour
  && hour < hours_per_day

let valid_minute min =
  min >= the_origin_of_time.minute
  && min < minutes_per_hour

let wellformed date =
  date.year >= 1
  && valid_month date.month
  && valid_day date.day
  && valid_hour date.hour
  && valid_minute date.minute

let to_int (d : date) : int =
  (d.year - 1) * minutes_per_year
  + (d.month - 1) * minutes_per_month
  + (d.day - 1) * minutes_per_day
  + d.hour * minutes_per_hour
  + d.minute

let of_int (n : int) : date =
  let year, n = n / minutes_per_year + 1, n mod minutes_per_year in
  let month, n = n / minutes_per_month + 1, n mod minutes_per_month in
  let day, n = n / minutes_per_day + 1, n mod minutes_per_day in
  let hour, n = n / minutes_per_hour, n mod minutes_per_hour in
  let minute = n in
  {year=year; month=month; day=day; hour=hour; minute=minute}

let next (date : date) : date =
  let minutes = (to_int date) in
  of_int (minutes + 1)
