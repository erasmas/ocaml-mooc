type trie = Trie of int option * char_to_children
and char_to_children = (char * trie) list

let empty =
  Trie (None, [])

let example =
  Trie (None,
        [('i', Trie (Some 11,
                    [('n', Trie (Some 5, [('n', Trie (Some 9, []))]))]));
        ('t', Trie (None,
                   [('e', Trie (None, [
                                ('n', Trie (Some 12, []));
                                ('d', Trie (Some 4, []));
                                ('a', Trie (Some 3, []))
                     ]));
                  ('o', Trie (Some 7, []))]));
        ('A', Trie (Some 15, []))])

let rec children_from_char (m : char_to_children) (c: char) : trie option =
  match m with
  | [] -> None
  | (ch, tr) :: tl -> if ch = c then Some tr else children_from_char tl c

let update_children (m : char_to_children) (c : char) (t : trie) : char_to_children =
  let t' = children_from_char m c in
  match t' with
  | None -> m @ [(c, t)]
  | _ -> List.map (fun (c', t') -> if c' = c then (c, t) else (c', t')) m ;;

let explode s =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) (s.[i] :: l) in
  exp (String.length s - 1) [];;

let lookup t s =
  let rec lookup' chars t = match (chars, t) with
    | _, None -> None
    | [], Some (Trie (None, _)) -> None
    | [], Some (Trie (Some v, _)) -> Some v
    | c :: cs, Some (Trie (_, children)) -> lookup' cs (children_from_char children c)
  in
  lookup' (explode s) (Some t)

let insert t s v =
  let rec insert' = function
    | [], Trie (_, children) -> Trie (Some v, children)
    | c :: cs, Trie (v, children) ->
      let t' = match (children_from_char children c) with
        | None -> empty
        | Some t -> t in
      let t'' = insert' (cs, t') in
      Trie (v, (update_children children c t'')) in
  insert' ((explode s), t)
