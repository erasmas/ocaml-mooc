let rec last_element = function
  | [] -> (invalid_arg "last_element")
  | x :: [] -> x
  | x :: xs -> last_element xs

let rec is_sorted = function
  | [] | [_] -> true
  | x :: y :: ys when (x < y) -> is_sorted (y :: ys)
  | x :: y :: _ -> false

let rec compose = function
  | [] -> (function x -> x)
  | [x] -> x
  | x :: xs -> (function y -> x ((compose xs) y))

let rec fixedpoint f start delta =
  let y = f start in
  let diff = abs_float (y -. (f y)) in
  if diff < delta then y
  else fixedpoint f y delta

let rec equal_on_common = function
  | [] -> (function _ -> true)
  | h1::r1 ->
    (function
      | [] -> true
      | h2 :: r2 -> h1 = h2 && (equal_on_common r1 r2))

(* ... *)

let ccr =
  let f = function x -> cos (x /. 2.) in
  function a ->
    let a' = 8. *. f a in
    function b ->
      let ab' = a' *. (f b) in
      function c ->
        let abc' = ab' *. (f c) in
        function s -> s /. abc'

(* ... *)

type operation =
    Op of string * operation * operation
  | Value of int

type env = (string * (int -> int -> int)) list

let rec lookup_function (n : string) = function
  | [] -> invalid_arg "lookup_function"
  | (name, op) :: fs -> if name = n then op else lookup_function n fs

let add_function name op env =
  (name, op) :: env

let my_env =
  [("min", (fun a b -> if a < b then a else b));
   ("add", (fun a b -> a + b));
   ("sub", (fun a b -> a - b));
   ("mul", (fun a b -> a * b));
   ("div", (fun a b -> a / b))]

let rec compute env = function
  | Value x -> x
  | Op (name, o1, o2) ->
    let f = lookup_function name env in
    f (compute env o1) (compute env o2)

let rec compute_eff env = function
  | Value x -> x
  | Op (name, o1, o2) ->
    ((lookup_function name env) (compute_eff env o1) (compute_eff env o2))

(* ... *)

type 'a tree =
    Node of 'a tree * 'a * 'a tree
  | Leaf of 'a;;

let rec tree_map f = function
  | Node (l, m, r) -> Node ((tree_map f l), f m, (tree_map f r))
  | Leaf a         -> Leaf (f a)

let wrap l =
  List.map (fun x -> [x]) l ;;

(* ... *)

let filter p l =
  List.fold_left (fun acc x -> if p x then (x :: acc) else acc) [] l;;

let partition p l =
  List.fold_right
    (fun x (tlst,flst)-> if p x then (x::tlst,flst) else (tlst,x::flst))
    l ([],[])

(* This is suboptimal because pivoting is done around the first element,
   and the function is not tail recursive.
 *)
let rec sort = function
  | [] -> []
  | x :: xs ->
    let (smaller, bigger) = partition (fun a -> a < x) xs in
    sort smaller @ (x :: sort bigger)

let generate_list n range : int list=
  let rec generate_list' (n : int) (acc : int list) =
    if (n = 0) then acc
    else generate_list' (n - 1) ((Random.int range) :: acc) in
  generate_list' n []

let time f x =
    let t = Unix.gettimeofday () in
    let fx = f x in
    Printf.printf "execution elapsed time: %f sec\n"
        (Unix.gettimeofday () -. t);
    fx

(* ... *)

let for_all p l =
  List.fold_left (fun acc x -> acc && p x) true l

let exists p l =
  List.fold_left (fun acc x -> if acc then acc else p x) false l

let sorted cmp = function
  | [] -> true
  | [x] -> true
  | x :: xs ->
    let sorted = List.fold_left
        (fun acc x -> match acc with
           | None -> None
           | Some v -> match cmp x v with
             | 0 | 1 -> Some x
             | _ -> None)
        (Some x) xs in
    match sorted with
    | Some _ -> true
    | None -> false
